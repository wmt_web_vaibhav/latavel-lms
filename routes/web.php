<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::group(['middleware'=>['web']],function (){
    Route::get('/', function () {
        return view('welcome')  ;
    });
    Route::resource('home','UserController');
    Route::get('deshboard','UserController@login');
    Route::post('deshboard','UserController@login');
    Route::get('/logout','UserController@logout');
    Route::resource('author','AuthorController');
    Route::post('active/{author}','AuthorController@active');
    Route::resource('book','BookController');
    Route::get('bactive/{book}','BookController@bactive');


});
