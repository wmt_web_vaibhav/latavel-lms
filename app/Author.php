<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Author extends Model
{
    protected $guarded=['status',];
    public function books(){
        return $this->hasMany(Book::class,'book_author','id');
    }
}
