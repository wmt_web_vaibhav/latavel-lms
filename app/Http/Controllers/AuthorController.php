<?php

namespace App\Http\Controllers;

use App\Author;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

class AuthorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $author=Author::all();
        return view('author',compact('author'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('addauthor');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|max:1000',
            'date' => 'required',
            'gender' => 'required',
            'address' => 'required',
            'mobileno' => 'required|digits:10|numeric',
            'description' => 'required',
        ]);
        $author=new Author();
        $author->name=$request->name;
        $author->dob=$request->date;
        $author->gender=$request->gender;
        $author->address=$request->address;
        $author->mobileno=$request->mobileno;
        $author->description=$request->description;
        $author->save();
        return redirect()->route('author.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $author=Author::find($id);
        return view('editauthor',compact('author'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required|max:1000',
            'date' => 'required',
            'gender' => 'required',
            'address' => 'required',
            'mobileno' => 'required|digits:10|numeric',
            'description' => 'required',
        ]);

        $author=Author::find($id);
        $author->name=$request->name;
        $author->dob=$request->date;
        $author->gender=$request->gender;
        $author->address=$request->address;
        $author->mobileno=$request->mobileno;
        $author->description=$request->description;
        $author->save();
        return redirect()->route('author.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $author = Author::find($id);
        $author->delete();
        return redirect()->route('author.index');
    }

    public function active($id){

        $author=Author::find($id);
        if ($author->status==1){
            $author->status=0;
        }else{
            $author->status=1;
        }
        $author->save();
        return redirect()->route('author.index');
    }
}
