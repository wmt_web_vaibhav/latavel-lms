<?php

namespace App\Http\Controllers;

use App\Author;
use App\Book;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class BookController extends Controller
{
    public function index()
    {
        $book=Book::all()->where('create_user',Auth::id());
        return view('book',compact('book'));
    }

    public function create()
    {
        $author=Author::all();
        return view('addbook',compact('author'));
    }

    public function store(Request $request)
    {

        $this->validate($request, [
            'title' => 'required|max:1000',
            'pages' => 'required|numeric',
            'language' => 'required',
            'book_author' => 'required',
            'cover_image' => 'required',
            'isbn' => 'required',
            'description' => 'required',
        ]);
        $book=new Book();
        $book->title=$request->title;
        $book->pages=$request->pages;
        $book->language=$request->language;
        $book->book_author=$request->book_author;
        if($request->hasFile('cover_image')){
            $file=$request->file('cover_image');
            $extension=$file->getClientOriginalExtension();
            $filename=time().'.'.$extension;
            $file->move('images/book_image/',$filename);
            $book->cover_image=$filename;
        }else{
            return $request;
            $book->cover_image='';
        }
        $book->isbn=$request->isbn;
        $book->description=$request->description;
        $book->create_user=Auth::id();
        $book->save();
        return redirect()->route('book.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $book=Book::find($id);
        return view('show',compact('book'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $book=Book::find($id);
        $author=Author::all();
        return view('editbook',compact('book','author'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'title' => 'required|max:1000',
            'pages' => 'required|numeric',
            'language' => 'required',
            'book_author' => 'required',
            'cover_image' => 'required',
            'isbn' => 'required',
            'description' => 'required',
        ]);

        $book=Book::find($id);
        $book->title=$request->title;
        $book->pages=$request->pages;
        $book->language=$request->language;
        $book->book_author=$request->book_author;


        if($request->has('image')) {
            $image = $request->file('image');
            $filename = $image->getClientOriginalName();
            $image->move('images/book_image', $filename);
            $book->image = $request->file('image')->getClientOriginalName();
        }
        $book->isbn=$request->isbn;
        $book->description=$request->description;
        $book->save();
        return redirect()->route('book.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $book = Book::find($id);
        $book->delete();
        return redirect()->route('book.index');
    }

    public function bactive($id){

        $book=Book::find($id);
        if ($book->status==1){
            $book->status=0;
        }else{
            $book->status=1;
        }
        $book->save();
        return redirect()->route('book.index');
    }
}
