<?php

namespace App;
use App\Author;
use Illuminate\Database\Eloquent\Model;

class Book extends Model
{
    public function authors(){
        return $this->belongsTo(Author::class,'book_author','id');
    }
}
