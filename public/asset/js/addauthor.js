$(document).ready(function() {
    $.validator.addMethod("mobile_no", function(value, element) {
        return this.optional(element) || /^(\+\d{1,3}[- ]?)?\d{10}$/.test(value);
    }, "Please specify a valid mobile number");

    jQuery.validator.addMethod("fname", function(value, element) {
        return this.optional(element) || value == value.match(/^[a-z][a-z\s]*$/);
    }, "Only Characters Allowed and space are allowed.");
    $("#form").validate({
        rules: {
            name: {
                required: true,
                fname: true
            },
            date: {
                required: true,
                date: true
            },

            address: {
                required: true
            },
            mobileno: {
                required: true,
                mobile_no: true
            },
            description: {
                required: true
            }
        }
    })
});
