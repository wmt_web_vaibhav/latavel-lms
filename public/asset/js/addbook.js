$(document).ready(function() {
    $.validator.addMethod("isbn", function(value, element) {
        return this.optional(element) || /^(?:ISBN(?:-1[03])?:? )?(?=[0-9X]{10}$|(?=(?:[0-9]+[- ]){3})[- 0-9X]{13}$|97[89][0-9]{10}$|(?=(?:[0-9]+[- ]){4})[- 0-9]{17}$)(?:97[89][- ]?)?[0-9]{1,5}[- ]?[0-9]+[- ]?[0-9]+[- ]?[0-9X]$/.test(value);
    }, "Please specify a valid isbn number");
    $("#form").validate({
        rules: {
            title: {
                required: true
            },
            page: {
                required: true
            },
            book_author: {
                required: true
            },
            language: {
                required: true
            },

            cover_img: {
                required: true
            },
            isbn_no: {
                required: true,
                isbn: true
            },
            description: {
                required: true
            }
        }
    })
});
