<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBooksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('books', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('title');
            $table->integer('pages');
            $table->string('language');
            $table->unsignedBigInteger('book_author');
            $table->foreign('book_author')->references('id')->on('authors')->onDelete('cascade')->deferrable('cascade');
            $table->string('cover_image');
            $table->string('isbn');
            $table->string('description');
            $table->tinyInteger('status')->default(1);
            $table->integer('create_user');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('books');
    }
}
