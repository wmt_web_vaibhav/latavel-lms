@extends('layouts.master')
@section('js')
    <script type="text/javascript" src="{{ asset('asset/js/addbook.js') }}" defer></script>
    <style>
        label.error{
            color: red;
        }
    </style>
    @endsection
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <h1 class="text-center card-title">Book</h1>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <form method="post" action="{{route('book.store')}}" id="form" enctype="multipart/form-data">
                    @csrf

                    <div class="form-group">
                        <label for="">Title</label>
                        <input type="text" name="title" id="title" class="form-control @error('title') is-invalid @enderror" placeholder="Enter Title" aria-describedby="helpId">
                        @error('title')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="">Pages</label>
                        <input type="number" name="pages" id="page" class="form-control @error('pages') is-invalid @enderror" placeholder="Enter Book Page" aria-describedby="helpId">
                        @error('pages')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="">Language</label>
                        <input type="text" name="language" id="language" class="form-control @error('language') is-invalid @enderror" placeholder="Enter Book Language" aria-describedby="helpId">
                        @error('language')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="">Book Author</label>
                        <select class="custom-select @error('book_author') is-invalid @enderror" name="book_author" id="book_author">
                            <option hidden value="">Book Author</option>
                            @foreach($author as $author)
                            <option value="{{ $author->id }}">{{ $author->name }}</option>
                            @endforeach
                        </select>
                        @error('book_author')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="">Cover image</label>
                        <input type="file" name="cover_image" id="cover_img" class="form-control @error('cover_image') is-invalid @enderror" placeholder="Enter Coverpage" aria-describedby="helpId">
                        @error('cover_image')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="">ISBN NO</label>
                        <input type="text" name="isbn" id="isbn" class="form-control @error('isbn') is-invalid @enderror" placeholder="Enter ISBN NO" aria-describedby="helpId">
                        <small class="text-muted">Ex: [1-2222-3333-4]</small>
                        @error('isbn')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="">Description</label>
                        <textarea class="form-control @error('description') is-invalid @enderror" id="description" name="description" rows="3"></textarea>
                        @error('description')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <input type="submit" name="insert" id="btn" class="btn btn-success" value="Submit" aria-describedby="helpId">
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
