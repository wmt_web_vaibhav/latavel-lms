@extends('layouts.master')
@section('content')
    <div>
        <a class="btn btn-info" href="{{route('author.create')}}">Add Author</a>
    </div>
    <div>
        <table class="table table-hover">
            <tr>
                <th>#</th>
                <th>NAME</th>
                <th>DOB</th>
                <th>GENDER</th>
                <th>ADDRESS</th>
                <th>MOBILE</th>
                <th>DESCRIPTION</th>
                <th>STATUS</th>
                <th>EDIT</th>
                <th>DELETE</th>
                <th>Active/Inactive</th>
            </tr>
            @foreach($author as $author)
                <tr>
                    <td>{{ $author->id }}</td>
                    <td>{{ $author->name }}</td>
                    <td>{{ $author->dob }}</td>
                    <td>{{ $author->gender }}</td>
                    <td>{{ $author->address }}</td>
                    <td>{{ $author->mobileno }}</td>
                    <td>{{ $author->description }}</td>
                    <td>{{ $author->status }}</td>
                    <td>
                        <form action="/author/{{$author->id}}/edit" method="post">
                            @csrf
                            @method('get')
                            <button type="submit" class="btn btn-success">Edit</button>
                        </form>
                    </td>
                    <td>
                        <form action="/author/{{$author->id}}" method="post">
                            @csrf
                            @method('delete')
                            <button type="submit" class="btn btn-danger">Delete</button>
                        </form>
                    </td>
                    <td>
                        <form action="/active/{{ $author->id }}" method="post">
                            @csrf
                            @if($author->status==1)
                                <button type="submit" class='btn btn-danger'>InActive</button>
                            @else
                                <button type="submit" class='btn btn-success'>Active</button>
                            @endif
                        </form>

                    </td>
                </tr>
            @endforeach

        </table>
    </div>
@endsection
