@extends('layouts.master')
@section('js')
    <script type="text/javascript" src="{{ asset('asset/js/addauthor.js') }}" defer></script>
    <style>
        .error{
            color: red;
        }
    </style>
@endsection
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <h1 class="text-center card-title">Author</h1>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <form method="post" action="{{route('author.store')}}" id="form">
                    @csrf
                    <div class="form-group">
                        <label for="">Name</label>
                        <input type="text" name="name" id="name" class="form-control @error('name') is-invalid @enderror" placeholder="Enter Name" aria-describedby="helpId">
                        @error('name')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="">Date Of Birth</label>
                        <input type="date" name="date" id="date" class="form-control @error('date') is-invalid @enderror" placeholder="Enter date" aria-describedby="helpId">
                        @error('date')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="">Gender</label>
                        <div class="form-check">
                            <input type="radio" class="form-check-input" id="gender" name="gender"value="male" checked>
                            <label class="form-check-label" for="materialUnchecked">Male</label>
                        </div>
                        <div class="form-check">
                            <input type="radio" class="form-check-input" id="gender" name="gender" value="female">
                            <label class="form-check-label" for="materialUnchecked">Female</label>
                        </div>
                        <div class="form-check">
                            <input type="radio" class="form-check-input" id="gender" name="gender" value="other">
                            <label class="form-check-label" for="materialChecked">Other</label>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="">Address</label>
                        <textarea class="form-control @error('address') is-invalid @enderror" name="address" id="address" rows="3" id="comment"></textarea>
                        <small class="error"></small>
                        @error('address')
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="">Mobile No</label>
                        <input type="text" name="mobileno" id="mobile" class="form-control @error('mobileno') is-invalid @enderror" placeholder="Enter Mobile No" aria-describedby="helpId">
                        <small class="error"></small>
                        @error('mobileno')
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="">Description</label>
                        <textarea class="form-control @error('description') is-invalid @enderror" name="description" id="description" rows="3" id="comment"></textarea>
                        <small class="error"></small>
                        @error('description')
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <input type="submit" name="insert" id="btn" class="btn btn-success" value="Submit" aria-describedby="helpId">
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
