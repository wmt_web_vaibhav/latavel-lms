@extends('layouts.master')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <h1 class="text-center card-title">Book</h1>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <form method="post" action="{{ route('book.update',[$book->id]) }}" id="form">
                    @csrf
                    @method('PUT')
                    <div class="form-group">
                        <label for="">Title</label>
                        <input type="text" name="title" id="title" value="{{ $book->title }}" class="form-control @error('title') is-invalid @enderror" aria-describedby="helpId">
                        @error('title')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="">Pages</label>
                        <input type="text" name="pages" id="page" value="{{ $book->pages }}" class="form-control @error('pages') is-invalid @enderror" aria-describedby="helpId">
                        @error('pages')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="">Language</label>
                        <input type="text" name="language" id="language" value="{{ $book->language }}" class="form-control @error('language') is-invalid @enderror" aria-describedby="helpId">
                        @error('language')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="">Book Author</label>
                        <select class="custom-select" name="book_author" id="book_author">
                            <option hidden>Book Author</option>
                            @foreach($author as $author)
                                <option value="{{ $author->id }}" {{ $book->book_author==$author->id?'selected':'' }}>{{ $author->name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="">Cover image</label>
                        <input type="file" name="cover_image" class="@error('cover_image') is-invalid @enderror" value=""/>
                        @error('cover_image')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="">ISBN NO</label>
                        <input type="text" name="isbn" id="isbn_no" value="{{ $book->isbn }}" class="form-control @error('isbn') is-invalid @enderror" aria-describedby="helpId">
                        <small class="text-muted">Ex: [1-2222-3333-4]</small>
                        @error('isbn')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="">Description</label>
                        <textarea class="form-control @error('description') is-invalid @enderror" id="description" name="description" rows="3">{{ $book->description }}</textarea>
                        @error('description')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <input type="submit" name="insert" id="btn" class="btn btn-success" value="Submit" aria-describedby="helpId">
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
