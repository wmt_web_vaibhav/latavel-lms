@extends('layouts.master')
@section('content')
    <div>
        <div class="card" style="width: 22rem;">
            <td><img class="card-img-top" src="{{asset('images/book_image/'.$book->cover_image)}}"></td>
            <div class="card-body">
                <h5 class="card-title"><b>Title : </b>{{ $book->title }}</h5>
                <h5 class="card-title"><b>Pages : </b>{{ $book->pages }}</h5>
                <h5 class="card-title"><b>Language : </b>{{ $book->language }}</h5>
                <h5 class="card-title"><b>Book Author : </b>{{ $book->authors->name }}</h5>
                <h5 class="card-title"><b>ISBN No. : </b>{{ $book->isbn }}</h5>
                <h5 class="card-title"><b>Description : </b>{{ $book->description }}</h5>
                <h5 class="card-title"><b>Status : </b>{{ $book->status }}</h5>
            </div>
        </div>
    </div>
@endsection
