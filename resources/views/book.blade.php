@extends('layouts.master')
@section('content')
    <div>
        <a class="btn btn-info" href="{{route('book.create')}}">Add Book</a>
    </div>
    <div>
        <table class="table table-hover">
            <tr>
                <th>#</th>
                <th>TITLE</th>
                <th>PAGES</th>
                <th>LANGUAGE</th>
                <th>AUTHOR NAME</th>
                <th>STATUS</th>
                <th>EDIT</th>
                <th>DELETE</th>
                <th>DETAIL</th>
                <th>Active/Inactive</th>
            </tr>
            @foreach($book as $book)
                <tr>
                    <td>{{ $book->id }}</td>
                    <td>{{ $book->title }}</td>
                    <td>{{ $book->pages }}</td>
                    <td>{{ $book->language }}</td>
                    <td>{{ $book->authors->name }}</td>
                    <td>{{ $book->status }}</td>
                    <td>
                        <form action="/book/{{$book->id}}/edit" method="post">
                            @csrf
                            @method('get')
                            <button type="submit" class="btn btn-success">Edit</button>
                        </form>
                    </td>
                    <td>
                        <form action="/book/{{$book->id}}" method="post">
                            @csrf
                            @method('delete')
                            <button type="submit" class="btn btn-danger">Delete</button>
                        </form>
                    </td>
                    <td>
                        <form action="/book/{{ $book->id }}" method="post">
                            @csrf
                            @method('get')
                            <button type="submit" class="btn btn-danger">DETAIL</button>
                        </form>
                    </td>
                    <td>
                        <form action="/bactive/{{ $book->id }}" method="get">
                            @csrf
                            @if($book->status==1)
                                <button type="submit" class='btn btn-danger'>InActive</button>
                            @else
                                <button type="submit" class='btn btn-success'>Active</button>
                            @endif
                        </form>
                    </td>
                </tr>
            @endforeach
        </table>
    </div>
@endsection
