@extends('layouts.master')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <h1 class="text-center card-title">Author</h1>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <form method="post" action="/author/{{$author->id}}" id="form">
                    @csrf
                    @method('put')
                    <div class="form-group">
                        <label for="">Name</label>
                        <input type="text" name="name" id="name" class="form-control" value="{{ $author->name }}" aria-describedby="helpId">
                    </div>
                    <div class="form-group">
                        <label for="">Date Of Birth</label>
                        <input type="date" name="date" id="date" class="form-control" value="{{ $author->dob }}" aria-describedby="helpId">
                    </div>
                    <div class="form-group">
                        <label for="">Gender</label>
                        <div class="form-check">
                            <input type="radio" class="form-check-input" id="gender" name="gender" value="male" {{ $author->gender=='male'? 'checked':'' }}>
                            <label class="form-check-label" for="materialUnchecked">Male</label>
                        </div>
                        <div class="form-check">
                            <input type="radio" class="form-check-input" id="gender" name="gender" value="female" {{ $author->gender=='female'? 'checked':'' }}>
                            <label class="form-check-label" for="materialUnchecked">Female</label>
                        </div>
                        <div class="form-check">
                            <input type="radio" class="form-check-input" id="gender" name="gender" value="other" {{ $author->gender=='other'? 'checked':'' }}>
                            <label class="form-check-label" for="materialChecked">Other</label>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="">Address</label>
                        <textarea class="form-control" name="address" id="address" rows="3" id="comment">{{ $author->address }}</textarea>
                    </div>
                    <div class="form-group">
                        <label for="">Mobile No</label>
                        <input type="text" name="mobileno" id="mobile" class="form-control" value="{{ $author->mobileno }}" aria-describedby="helpId">
                    </div>
                    <div class="form-group">
                        <label for="">Description</label>
                        <textarea class="form-control" name="description" id="description" rows="3" id="comment">{{ $author->description }}</textarea>
                    </div>
                    <div class="form-group">
                        <input type="submit" name="insert" id="btn" class="btn btn-success" value="Submit" aria-describedby="helpId">
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
