@extends('layouts.master')

@section('title')
    welcome!!!
@endsection

@section('content')
    <div class="row my-5">
        <div class="col-sm-6">
            <h2>Sign Up</h2>
            <form action="{{ route('home.store') }}" method="post">
                @csrf
                <div class="form-group">
                    <label for="email">Email : </label>
                    <input type="email" class="form-control @error('email') is-invalid @enderror" name="email" id="email">
                    @error('email')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="name">Name : </label>
                    <input type="text" class="form-control @error('name') is-invalid @enderror" name="name" id="name">
                    @error('name')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror

                </div>
                <div class="form-group">
                    <label for="password">Password : </label>
                    <input type="password" class="form-control @error('password') is-invalid @enderror" name="password" id="password">
                    @error('password')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>
                <button type="submit" class="btn btn-primary">Submit</button>
            </form>
        </div>
        <div class="col-sm-6">
            <h2>Sign In</h2>
            <form action="/deshboard" method="post">
                @csrf
                <div class="form-group">
                    <label for="email">Email : </label>
                    <input type="email" class="form-control @error('email1') is-invalid @enderror" name="email1" id="email">
                    @error('email1')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="password">Password : </label>
                    <input type="password" class="form-control @error('password1') is-invalid @enderror" name="password1" id="password">
                    @error('password1')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>
                <button type="submit" class="btn btn-primary">Submit</button>
            </form>
        </div>
    </div>
@endsection
